/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gis1.schudule.cordo.metiers;

import com.gis1.schudule.ResultStatut;
import com.gis1.schudule.cordo.datalayers.FaculteDataLayers;
import com.gis1.schudule.cordo.entities.Faculte;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ppitbull
 */
@Service
public class FaculteMetier {
    private FaculteDataLayers dataLayer;

    @Autowired
    public FaculteMetier(FaculteDataLayers dataLayer) {
        this.dataLayer = dataLayer;
    }

    
    
    public ResultStatut addFaculte(Faculte newFaculte)
    {
        return this.dataLayer.addNewFaculte(newFaculte);
    }
    
    public ResultStatut getFaculteList()
    {
        return this.dataLayer.getFaculteList();
    }
}
