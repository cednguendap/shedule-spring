/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gis1.schudule.cordo.services;

import com.gis1.schudule.ResultStatut;
import com.gis1.schudule.cordo.entities.Faculte;
import com.gis1.schudule.cordo.metiers.FaculteMetier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ppitbull
 */
@RestController
@RequestMapping(path="faculte")
public class FaculteService 
{
    private FaculteMetier faculteMetier;

    @Autowired
    public FaculteService(FaculteMetier faculteMetier) {
        this.faculteMetier = faculteMetier;
    }

    
    
    
    @PostMapping("/new")
    public ResultStatut postFaculte(@RequestBody Faculte faculte) {  
        ResultStatut result= this.faculteMetier.addFaculte(faculte);
        return result;
    }
    
    @GetMapping("/list")
    public ResultStatut getFaculteList()
    {
        return this.faculteMetier.getFaculteList();
    }
}
