/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gis1.schudule.cordo.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author ppitbull
 */
@Entity
public class Faculte implements Serializable {
    
    @Id
    private String id;
    private String nom;
    private String description;
    
    
    private String sampleProperty;
    
    
    public Faculte() {
    }
    public Faculte(String id, String nom, String description) {
        this.id = id;
        this.nom = nom;
        this.description = description;

    }
    public String getSampleProperty() {
        return sampleProperty;
    }

    public String getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getDescription() {
        return description;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
