/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gis1.schudule.cordo.datalayers;

import com.gis1.schudule.ResultStatut;
import com.gis1.schudule.cordo.entities.Faculte;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author ppitbull
 */
@Service
public class FaculteDataLayers {
    List<Faculte> faculteList = new ArrayList<Faculte>();
    
    FaculteDataLayers()
    {
        this.faculteList.add(new Faculte("1223","FST","Faculté des sciences et des technologies"));
        this.faculteList.add(new Faculte("1224","FSS","Faculté des sciences et de la santé"));

    }
    
    public ResultStatut addNewFaculte(Faculte faculte)
    {
        this.faculteList.add(faculte);
        
        ResultStatut result=new ResultStatut();
        result.setStatut(0);
        return result;
    }
    
    public ResultStatut getFaculteList()
    {
        ResultStatut result = new ResultStatut();
        result.setResult((this.faculteList));
        return result;
    }
}
