/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gis1.schudule;

/**
 *
 * @author ppitbull
 */
public class ResultStatut {
    private Integer statut;
    private String message;
    private Object result;

    public ResultStatut(Integer statut, String message, Object result) {
        this.statut = statut;
        this.message = message;
        this.result = result;
    }

    public ResultStatut() {
        this.statut=0;
        this.message="success";
        this.result=null;
    }
    
    public Integer getStatut() {
        return statut;
    }

    public String getMessage() {
        return message;
    }

    public Object getResult() {
        return result;
    }

    public void setStatut(Integer statut) {
        this.statut = statut;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setResult(Object result) {
        this.result = result;
    }
    
}
